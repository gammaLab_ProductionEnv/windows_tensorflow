# Windows环境下编译Tensorflow1.6.0 C++ library
最近由于项目原因，接触了OSX环境下的java调用tensorflow模型生产化部署，觉得特别好用，又听闻之前有同事花了两周时间，没有编译成功Tensorflow在windows下的C++ library, 但我出于大厂出品应属精品的信念，顺便做一下这方面的储备，断断续续花了三晚终于折腾成功，人都搞黑了。一方面国内网速不行，另一方面Tensorflow的Cmake编译脚本确实不好用。不过总体思路也就是挨个填坑，人挡杀人佛挡杀佛。

## Step1: Windows 10环境配置
* 安装VS2015/ VS2017;
* 安装Swigwin-3.0.12，添加环境变量;
* 安装python-3.5+, 添加全局环境变量;
* 安装CMake-3.8.0, 添加环境变量;
* 安装Git, 编译过程中需要下载其他项目依赖, 生成工程需要用到GitBash;
* 从GitHub上下载Tensorflow的Master分支到本地;
        
## Step2: 修改编译配置CMakeList.txt
如果是StandAlone版本并且不需要gRPC模块的话,强烈建议将该文件22行的gRPC模块支持关闭，修改如下:

option(tensorflow_ENABLE_GRPC_SUPPORT "Enable gRPC support" OFF)

因为我朝特色,即使翻墙也很难下载成功,处座在这个包上浪费了一个晚上。一般项目如果需要定制化模块，可以通过修改此处的编译配置文件，通过开关关闭一些项目不需要的第三方依赖。


## Step3: 生成Visual Studio工程
* 打开桌面上的 GitBash命令终端；
* 进入目录 F:\tensorflow\tensorflow\contrib\cmake;
* 新建文件夹mkdir build_2015;

最后命令行输入编译指令：
 
 cmake .. -A x64 -DCMAKE_BUILD_TYPE=Release -DSWIG_EXECUTABLE=F:/swigwin-3.0.12/swig.exe -DPYTHON_EXECUTABLE=C:/Program\ Files/Anaconda3/python.exe -DPYTHON_LIBRARIES=C:/Program\ Files/Anaconda3/libs/python35/lib -Dtensorflow_BUILD_SHARED_LIB=ON;

 编译成功就在目录build_2015/下生成了tensorflow.sln工程

 ## Step4: 修改第三方依赖的编译配置文件
 直接打开tensorflow.sln工程，编译会出现数百个错误，主要还是因为第三方依赖库的编译不成功导致的。 有一些依赖库编译失败是由于VS调用git submodule init失败，时间短暂，没时间纠结VS和git的爱恨情仇。 所以直接改了第三方依赖的配置文件，删除了对submodule的调用。如果第三方依赖库调用出问题，后续可以单独编译该依赖库即可(仅提供思路, 未验证)。

 ### 修改1:re模块
打开\build_2015\re2\src\re2\CMakeLists.txt，将第16行修改为:

option(RE2_BUILD_TESTING “enable testing for RE2” OFF)

 ### 修改2: 删除第三方 git submodule模块
第三方依赖库编译涉及到submodule的都会在执行git submodule init的错误, 应该是VS和git之间的bug, 但是处座没有在此纠结，简化处理办法就是将这些submodule的命令全部删除，涉及到submodule的第三方库有:highwayhash, jsoncpp, nsync, protobuf, re2。
以re2依赖库为例,修改办法:打开build_2015\re2\tmp\re2-gitclone.cmake,删除Line78-94:

execute_process(
  COMMAND "C:/Program Files/Git/mingw64/bin/git.exe" ${git_options} submodule init 
  WORKING_DIRECTORY "F:/tensorflow/tensorflow/contrib/cmake/build_2015_org/re2/src/re2"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to init submodules in: 'F:/tensorflow/tensorflow/contrib/cmake/build_2015_org/re2/src/re2'")
endif()

execute_process(
  COMMAND "C:/Program Files/Git/mingw64/bin/git.exe" ${git_options} submodule update --recursive --init 
  WORKING_DIRECTORY "F:/tensorflow/tensorflow/contrib/cmake/build_2015_org/re2/src/re2"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR "Failed to update submodules in: 'F:/tensorflow/tensorflow/contrib/cmake/build_2015_org/re2/src/re2'")
endif()

其他第三方依赖库需要对/tmp/xxx-gitclone.cmake文件进行修改，通过搜索submodule定位关键字即可。

## Step5: 编译工程
* 打开tensorflow.sln工程，选择Release+x64编译选项，就可以执行编译了，如果期间出现错误，按照上述步骤排查重新编译；
* 如果需要Debug版本，只要把步骤3的cmake命令中的python.lib修改为python_d.lib即可;
* 编译成功后, 打开cmake\build_2015\Release;

## Step6: 测试工程
后期再放如何配置并且调用，亲测有效。有任何遗漏步骤，欢迎补充。

Email: mefwei@163.com

## 参考链接：
https://www.cnblogs.com/jliangqiu2016/p/7642471.html